import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, Routes } from '@angular/router';
import { AuthService } from 'src/app/service/AuthService';
import { MovieSerieService } from 'src/app/service/MovieSerieService';

@Component({
  selector: 'app-ingresar',
  templateUrl: './ingresar.component.html',
  styleUrls: ['./ingresar.component.css']
})
export class IngresarComponent implements OnInit {
  miFormulario: FormGroup = this.formulario.group({
  email: [, [Validators.required, Validators.minLength(6)]],
  pass: [, [Validators.required,Validators.min(6)]],
  });
 
  test : any

  constructor( private formulario : FormBuilder, private _authService : AuthService, private router:Router, private _movieserieservice : MovieSerieService) { }

  ngOnInit(): void {
  }


campoValido(campo: string){
  return(
    this.miFormulario.controls[campo].errors &&
    this.miFormulario.controls[campo].touched
  );
}

guardar(){
  if(this.miFormulario.invalid){
    this.miFormulario.markAllAsTouched();
    return
  }
  
}

async Ingresar (){
  const{ email, pass } = this.miFormulario.value;
  try {
    const user = await this._authService.login(email, pass);
     if (user){
       localStorage.setItem('usuario', JSON.stringify(user.user))
       this.router.navigate(['/Inicio']);
       this.test = localStorage.getItem ("usuario");
       this.test = JSON.parse (this.test)
       this._movieserieservice.addUser (this.test.uid)
     } 
    } catch (err){
      console.log(err)
    } 
  }

async IngresarConGoogle(){
  const{ email, pass } = this.miFormulario.value;
  try {
    const user = await this._authService.loginWithGoogle(email, pass);
    if (user){
      localStorage.setItem('usuario', JSON.stringify(user.user))
      this.router.navigate(['/Inicio']);
      this.test = localStorage.getItem ("usuario");
      this.test = JSON.parse (this.test)
      this._movieserieservice.addUser (this.test.uid)
    } 
   } catch (err){
     console.log(err)
   } 
}

}
