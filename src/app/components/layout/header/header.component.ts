import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/service/AuthService';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  public userLogged : Observable <any> = this._authservice.afauth.user;

  constructor(private _authservice : AuthService) { }

   

  ngOnInit(): void {
  }


  public logOut(){
    this._authservice.logout();
  }
}
