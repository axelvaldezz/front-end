import { Component, Input, OnInit } from '@angular/core';
import { MovieSerieBase, Trending } from 'src/app/interface/Trending';
import { MovieSerieService } from 'src/app/service/MovieSerieService';


@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {

   @Input() movies_series! : MovieSerieBase 
   @Input() isadd : Boolean = false

   user : any

  constructor(private _movieserieservice : MovieSerieService) { }

  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem("usuario")!)
    console.log ("mensajee: ", this.movies_series)
  }

  addItem(){
 
   this._movieserieservice.addItem(this.user.uid, this.movies_series).then(
      () => {
        console.log("se agrego a la bd: ",this.user.uid, this.movies_series)
      }
   )
  }

  deleteItem() {
    this._movieserieservice.deleteItem(this.user.uid, this.movies_series.idGlobal).then( () => {
      console.log('se elimino correctamente')
    }).catch ( error => {
      console.log
    })
   }

}
